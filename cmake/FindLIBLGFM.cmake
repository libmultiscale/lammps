# - Try to find LibXml2
# Once done this will define
#  LIBXML2_FOUND - System has LibXml2
#  LIBXML2_INCLUDE_DIRS - The LibXml2 include directories
#  LIBXML2_LIBRARIES - The libraries needed to use LibXml2
#  LIBXML2_DEFINITIONS - Compiler switches required for using LibXml2

# find_package(PkgConfig)
# pkg_check_modules(PC_LIBLGFM QUIET libxml-2.0)
# set(LIBXML2_DEFINITIONS ${PC_LIBXML_CFLAGS_OTHER})

# Set directory of libLGFM
set(DIR_LGFM ${CMAKE_MODULE_PATH})
get_filename_component(DIR_LGFM ../liblgfm ABSOLUTE)
# message("LGFM dir = ${DIR_LGFM}")

set( LIBLGFM_INCLUDE_DIRS
    ${DIR_LGFM}/coeffs
  ${DIR_LGFM}/GF
  ${DIR_LGFM}/DBemTools
  ${DIR_LGFM}/helper
  ${DIR_LGFM}/matrix
  ${DIR_LGFM}/matrix/H
  ${DIR_LGFM}/matrix/H/AHMED
  ${DIR_LGFM}/extern/ahmed/Include
)

find_path( LIBLGFM_INCLUDE_DIR
  NAMES lgfm_package.h
  HINTS ${DIR_LGFM}
  PATH_SUFFIXES helper
)

# Set directory where to find liblgfm.so
set(DIR_LGFM_LIBRARY ${DIR_LGFM})
get_filename_component(DIR_LGFM_LIBRARY ../../build/third-party/liblgfm ABSOLUTE)
# message("LGFM library dir = ${DIR_LGFM_LIBRARY}")

find_library( LIBLGFM_LIBRARY
  NAMES lgfm liblgfm
  HINTS ${DIR_LGFM_LIBRARY}
)

# include(FindPackageHandleStandardArgs)
# # handle the QUIETLY and REQUIRED arguments and set LIBXML2_FOUND to TRUE
# # if all listed variables are TRUE
# find_package_handle_standard_args(LibXml2  DEFAULT_MSG
#                                   LIBXML2_LIBRARY LIBXML2_INCLUDE_DIR)

# mark_as_advanced(LIBXML2_INCLUDE_DIR LIBXML2_LIBRARY )

set( LIBLGFM_LIBRARIES ${LIBLGFM_LIBRARY} )
# set( LIBLGFM_INCLUDE_DIRS ${LIBLGFM_INCLUDE_DIR} )
