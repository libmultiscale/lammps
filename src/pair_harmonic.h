/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under 
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef PAIR_CLASS

PairStyle(k/req,PairHarmonic)

#else

#ifndef LMP_PAIR_HARMONIC_H
#define LMP_PAIR_HARMONIC_H

#include "pair.h"

namespace LAMMPS_NS {

class PairHarmonic : public Pair {
 public:
  PairHarmonic(class LAMMPS *);
  virtual ~PairHarmonic();
  virtual void compute(int, int);
  void settings(int, char **);
  void coeff(int, char **);
  void init_style();
  double init_one(int, int);
  void write_restart(FILE *);
  void read_restart(FILE *);
  void write_restart_settings(FILE *);
  void read_restart_settings(FILE *);
  void *extract(char *, int &);

 protected:
  double cut_global;
  double **cut;
  double **stiffness,**spring_length;
  double **offset;

  void allocate();
};

}

#endif
#endif
