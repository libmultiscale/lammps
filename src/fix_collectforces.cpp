/* ----------------------------------------------------------------------
   Modified version of LAMMPS for CollectForces simulations
   
   Copyright (2011) EPFL, LSMS, Peter Spijker
------------------------------------------------------------------------- */

#include "math.h"
#include "stdlib.h"
#include "string.h"
#include "fix_collectforces.h"
#include "atom.h"
#include "update.h"
#include "respa.h"
#include "force.h"
#include "domain.h"
#include "error.h"
#include "memory.h"
#include "group.h"
#include "pair_lj_charmm_coul_long.h"
#include "pair_hybrid.h"
#include "glob.h"
#include "modify.h"

using namespace LAMMPS_NS;

static const unsigned int root_proc = 0;



/* ---------------------------------------------------------------------- */

/* The constructor */
FixCollectForces::FixCollectForces(LAMMPS *lmp, int narg, char **arg) :
  Fix(lmp, narg, arg)
{
  /* Set the MPI environment */
  MPI_Comm_rank(world,&me);
  MPI_Comm_size(world,&nprocs);

  /* Command is of form: fix id group collectforces */
  if (narg != 3) error->all("Illegal fix collectforces command (wrong number of arguments)");

  /* See if the universe exists (Genesis) */
  if (domain->box_exist == 0) error->all("Fix collectforces command before simulation box is defined");
  if (atom->natoms == 0) error->all("Fix collectforces command with no atoms existing");
  if (domain->triclinic) error->all("Fix collectforces command needs an orthorhombic box, not triclinic");

  /* Make sure that we use Verlet (otherwise forces are not collected) */
  if (strcmp(update->integrate_style,"verlet")!=0) error->all("Fix collectforces command needs the verlet integration");

  /* Turn newton_pair off, so the forces of the local particles are completely 
     known and no extra communication has to be done. In this case we use as a
     function to build neighbor lists half_bin_no_newton */
  if (force->newton_pair) error->all("Fix collectforces command needs newton_pair to be turned off (to collect correct forces)");

  /* How many time steps do we use to average the global energy and force array (per type) */
  //avgtime = atoi(arg[4]);

  /* Some debug printing */
  if (me == root_proc) {
    fprintf(screen,">> fix collectforces - options :\n");
  }

  /* Make sure that we use the pair style that allows for the force collection */
  int i, j, k, m;
  pairstyle = new char[strlen(force->pair_style)+1];
  strcpy(pairstyle,force->pair_style);
  if (strcmp(pairstyle,"hybrid")==0) {
    int nstyles;
    PairHybrid *ph = (PairHybrid*)force->pair;
    nstyles = ph->nstyles;
    if (me == root_proc) 
      fprintf(screen,"   * pair style: %s with %d styles\n",force->pair_style,nstyles);

    for (i=0, j=0; i<nstyles; ++i) { 
      if (me == root_proc) fprintf(screen,"     - %s",ph->keywords[i]);
      if (strcmp(ph->keywords[i],"lj/charmm/coul/long")==0) { 
	j = 1; 
	if (me == root_proc) fprintf(screen," (OK!)\n"); 
      }
      else if (strcmp(ph->keywords[i],"lj/cut")==0) { 
	j = 1; 
	if (me == root_proc) fprintf(screen," (OK!)\n"); 
      }
      else { 
	if (me == root_proc) fprintf(screen,"\n"); 
      }
    }
    if (j==0)
      error->all("Fix collectforces command needs at least on of the hybrid pair_styles to be lj/cut or  lj/charmm/coul/long");
  }
  else {
    j=0;
    if (me == root_proc) fprintf(screen,"   * pair style: %s - ",force->pair_style);
    if (strcmp(pairstyle,"lj/charmm/coul/long")!=0) { 
      j = 1; 
      if (me == root_proc) fprintf(screen,"(OK!)\n");  
    }
    else if (strcmp(pairstyle,"lj/cut")!=0) { 
      j = 1; 
      if (me == root_proc) fprintf(screen,"(OK!)\n");  
    }
    if (j==0) {
      error->all("Fix collectforces command needs the pair_styles to be lj/cut or lj/charmm/coul/long");
    }
  }

  /* Initialize some arrays */
  int ntypes = atom->ntypes;
  n = ntypes+1;
  nn = n*n;
  ForcePerType = new double[nn*3];

  /* Do this every step */
  nevery = 1;


  array_flag = 1;
  size_array_rows = ntypes*3;
  size_array_cols = ntypes;
  force_clear(atom->ntypes);
}

/* ---------------------------------------------------------------------- */

/* The destructor */
FixCollectForces::~FixCollectForces()
{
  delete [] ForcePerType;
  return;
}

/* ---------------------------------------------------------------------- */

/* When do we call our functions? */
int FixCollectForces::setmask()
{
  int mask = 0;
  /* (see modify.cpp) */
  mask |= PRE_FORCE;
  return mask;
}

/* ---------------------------------------------------------------------- */

/* Do some stuff (only on the first step) */
void FixCollectForces::setup(int vflag)
{
  return;
}

/* ---------------------------------------------------------------------- */

/* Initialize the force collection in every iteration */
void FixCollectForces::pre_force(int vflag)
{
  force_clear(atom->ntypes);
  return;
}

/* ---------------------------------------------------------------------- */

#define ACCESS(itype,jtype,dim) (3*((itype)*n + (jtype))+dim)

#include <iomanip>

double FixCollectForces::compute_array(int i, int j)
{
  force_reduce();
  int itype = i%atom->ntypes;
  int dim = i/atom->ntypes;

  return ForcePerType[ACCESS(itype+1,j+1,dim)];
}
#undef ACCESS
/* -------------------------------------------------------------------------- */


/* Clear the force array */
void FixCollectForces::force_clear(int ntypes)
{
  memset(ForcePerType,0,sizeof(double)*nn*3);
  reduce_done = false;
  return;
}
/* -------------------------------------------------------------------------- */

/* Collect forces across all processors */
void FixCollectForces::force_reduce()
{
  if (reduce_done) return;
  reduce_done = true;

  double *glob;

  /* Allocate memory (combine energy and force) */
  glob = (double *)calloc(3*nn,sizeof(double));

  /* Now collect the reduced force matrix on all processors */
  MPI_Allreduce(ForcePerType,glob,3*nn,MPI_DOUBLE,MPI_SUM,world);

  memcpy(ForcePerType,glob,3*nn*sizeof(double));

  /* Remove the transmission arrays */
  free(glob);

  return;
}

/* ---------------------------------------------------------------------- */


