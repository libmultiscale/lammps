/* ----------------------------------------------------------------------
   Modified version of LAMMPS for AFM simulations
   
   Copyright (2011) EPFL, LSMS, Peter Spijker
------------------------------------------------------------------------- */

#ifdef FIX_CLASS

FixStyle(collect,FixCollectForces)

#else

#ifndef FIX_COLLECTFORCES_H
#define FIX_COLLECTFORCES_H

#include "fix.h"

namespace LAMMPS_NS {

  class FixCollectForces : public Fix {
  public:
    /* Constructor and destructor */
    FixCollectForces(class LAMMPS *, int, char **);
    ~FixCollectForces();

    /* The base functions (controlled by fix.cpp) */
    int setmask();
    void setup(int);
    void pre_force(int);

    /* Force collection functions (called in pair_lj_afm.cpp) */
    void force_clear(int);
    static inline void force_add(double *, int , bool, int, int, double, double, double);
    void force_reduce();

    double compute_array(int,int);

    double *ForcePerType;   /* Force per atom split per type     */


  private:

    int me;                   /* The current processor ID          */
    int nprocs;               /* Total number of processors        */
    char *pairstyle;          /* The pair style in use             */

    bool reduce_done;

    int n;
    int nn;
  };


/* -------------------------------------------------------------------------- */
#define ACCESS(itype,jtype,dim) (3*((itype)*n + (jtype))+dim)

  /* Add the separated forces to the correct bin */
  inline void FixCollectForces::force_add(double * ForcePerType, int n, bool reverse_interaction,
					  int itype, int jtype, 
					  double frcx, double frcy, double frcz)
  {
    ForcePerType[ACCESS(itype,jtype,0)] += frcx;  /* Force on i in x-direction        */
    ForcePerType[ACCESS(itype,jtype,1)] += frcy;  /* Force on i in y-direction        */
    ForcePerType[ACCESS(itype,jtype,2)] += frcz;  /* Force on i in z-direction        */
    
    if (reverse_interaction){
      ForcePerType[ACCESS(jtype,itype,0)] -= frcx;  /* Force on j in x-direction        */
      ForcePerType[ACCESS(jtype,itype,1)] -= frcy;  /* Force on j in y-direction        */
      ForcePerType[ACCESS(jtype,itype,2)] -= frcz;  /* Force on j in z-direction        */
    }
    return;
  }
#undef ACCESS
}


/* -------------------------------------------------------------------------- */



#endif
#endif
